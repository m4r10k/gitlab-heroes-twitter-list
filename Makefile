GO ?= GO111MODULE=on CGO_ENABLED=0 go
PACKAGES = $(shell go list ./... | grep -v /vendor/)
GIT_COMMIT:=$(shell git describe --dirty --always)
IMAGE_REGISTRY := m4r10k
IMAGE_NAME := gitlab-heroes-twitter-list
.PHONY: all
all: install

.PHONY: clean
clean:
	$(GO) clean -i ./...
	rm -rf bin/

.PHONY: install
install:
	$(GO) install -ldflags "-s -w -X gitlab.com/m4r10k/gitlab-heroes-twitter-list/version.REVISION=$(GIT_COMMIT)" -v

.PHONY: build
build:
	$(GO) build  -ldflags "-s -w -X gitlab.com/m4r10k/gitlab-heroes-twitter-list/version.REVISION=$(GIT_COMMIT)" -v -o bin/gitlab-heroes-twitter-list

.PHONY: fmt
fmt:
	$(GO) fmt $(PACKAGES)

.PHONY: vet
vet:
	$(GO) vet $(PACKAGES)

.PHONY: lint
lint:
	@which golint > /dev/null; if [ $$? -ne 0 ]; then \
		$(GO) get -u golang.org/x/lint/golint; \
	fi
	for PKG in $(PACKAGES); do golint -set_exit_status $$PKG || exit 1; done;


.PHONY: docker
docker:
	docker image build -t $(IMAGE_REGISTRY)/$(IMAGE_NAME):$(GIT_COMMIT) .


.PHONY: docker
docker-push:
	docker push $(IMAGE_REGISTRY)/$(IMAGE_NAME):$(GIT_COMMIT) 


.PHONY: run
run: docker
	docker run -it --rm $(IMAGE_REGISTRY)/$(IMAGE_NAME):$(GIT_COMMIT)
