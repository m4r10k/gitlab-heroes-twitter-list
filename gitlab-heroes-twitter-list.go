package main

import (
	"bufio"
	"io"
	"log"
	"net/http"
	"os"
	"regexp"
	"sort"
	"strings"

	"github.com/dghubble/go-twitter/twitter"
	"github.com/dghubble/oauth1"
)

func main() {

	// Request the HTML page.
	// Get the slugs from GitLab
	res, err := http.Get("https://gitlab.com/gitlab-com/www-gitlab-com/raw/master/data/heroes.yml")
	if err != nil {
		log.Fatal(err)
	}

	defer res.Body.Close()

	if res.StatusCode != 200 {
		log.Fatalf("status code error: %d %s", res.StatusCode, res.Status)
	}

	reader := bufio.NewReader(res.Body)
	var slugs []string
	for {
		line, _, err := reader.ReadLine()

		if err == io.EOF {
			break
		}

		matched, err := regexp.MatchString(`.*twitter.*`, string(line))
		if err != nil {
			log.Fatalln(err)
		}

		if matched {
			s := strings.Split(string(line), ":")
			cleaned := strings.TrimSpace(s[1])
			if cleaned != "" {
				slugs = append(slugs, strings.ToLower(cleaned))
			}
		}
	}

	log.Printf("GitLab member count: %+v", len(slugs))
	log.Printf("GitLab member list: %+v", slugs)

	// Get the members from the existing list

	config := oauth1.NewConfig(os.Getenv("CONSUMERKEY"), os.Getenv("CONSUMERSECRET"))
	token := oauth1.NewToken(os.Getenv("ACCESSTOKEN"), os.Getenv("ACCESSSECRET"))
	httpClient := config.Client(oauth1.NoContext, token)

	// Twitter client
	client := twitter.NewClient(httpClient)

	var listMembers []string

	members, _, err := client.Lists.Members(&twitter.ListsMembersParams{ListID: 1221901753425309698})

	if err != nil {
		log.Fatalln(err)
	}

	for _, m := range members.Users {
		listMembers = append(listMembers, strings.ToLower(m.ScreenName))
	}

	nc := members.NextCursor

	for nc > 0 {
		if members.NextCursor != 0 {
			members, _, err := client.Lists.Members(&twitter.ListsMembersParams{ListID: 1221901753425309698, Cursor: nc})

			if err != nil {
				log.Fatalln(err)
			}
			for _, m := range members.Users {
				listMembers = append(listMembers, strings.ToLower(m.ScreenName))
			}
			nc = members.NextCursor
		}
	}

	sort.Strings(listMembers)

	log.Printf("Twitter member count: %+v", len(listMembers))
	log.Printf("Twitter member list: %+v", listMembers)

	// compare the lists
	var missingmembers []string
	for _, ss := range slugs {
		found := false
		for _, tt := range listMembers {
			if ss == tt {
				found = true
			}
		}
		if found == false {
			missingmembers = append(missingmembers, ss)
		}

	}

	log.Printf("Missing members: %+v", missingmembers)

	// Add member to list
	debug := os.Getenv("DEBUG")
	for _, slug := range missingmembers {

		if debug == "false" {
			resp, err := client.Lists.MembersCreate(&twitter.ListsMembersCreateParams{ListID: 1221901753425309698, ScreenName: slug})

			if err != nil {
				log.Fatalln(err)
			}

			log.Printf("Slug: %s - Response: %s", slug, resp.Status)

		} else {
			log.Printf("Debug slug: %s", slug)
			resp, _ := client.Lists.MembersCreate(&twitter.ListsMembersCreateParams{ListID: 1221901753425309698, ScreenName: "m4r10k"})
			log.Println(resp)
			break
		}

	}

}
