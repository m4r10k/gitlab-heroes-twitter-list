# gitlab-heroes-twitter-list

![](gitlab-heroes.png)

This is simple Golang based utility to maintain a Twitter list for the GitLab Heroes automatically per daily pipeline run. *The goal is, that we have a Twitter list, which other interested people can follow easily.*

The utility uses some tools to read the [GitLab Heroes Web Page](https://about.gitlab.com/community/heroes/members/) an extract the Twitter handles (slugs) from there. This is done via the [goquery](https://github.com/PuerkitoBio/goquery) library - very nice interface for JQuery like data selection!

Afterwards, the Twitter API is used to add the user slugs to the Twitter list. Currently no delta is generated - therefore it will only add users, but it will not delete them. The code doesn't check if the user slug is already a member of the list, because Twitter does only notify the user on the first add.

Twitter integration is done via the [go-twitter](https://github.com/dghubble/go-twitter) library which is very useful for this!

The daily run is scheduled via GitLab pipelining.
